module.exports = [

  {
    id: 10,

    name: '记录',
    icon: 'setting',
    router: '/record',

  },
  {
    id: 11,
    name: '统计',
    icon: 'setting',
    router: '/statistics',
  },
  {
    id: 8,
    name: '字典',
    icon: 'setting',
    router: '/dict',

  },
  {
    id: 9,
    name: '设备',
    icon: 'setting',
    router: '/device',

  },

]
