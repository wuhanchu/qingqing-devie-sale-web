import { login } from '../services/app'
import { routerRedux } from 'dva/router'
import { queryURL } from '../utils'

export default {
  namespace: 'login',
  state: {
    loginLoading: false,
  },

  effects: {
    * login ({
      payload,
    }, { put, call }) {
      yield put({ type: 'showLoginLoading' })
      const token = yield call(login, payload)
      yield put({ type: 'hideLoginLoading' })
      if (token.data) {
        token.data.expires = token.expires.getTime()
        sessionStorage.setItem('token', JSON.stringify(token.data))
        sessionStorage.setItem('company', payload.company)
;
        const from = queryURL('from')
        yield put({ type: 'app/query' })
        if (from) {
          yield put(routerRedux.push(from))
        } else {
          yield put(routerRedux.push('/record'))
        }
      } else {
        throw token
      }
    },
  },
  reducers: {
    showLoginLoading (state) {
      return {
        ...state,
        loginLoading: true,
      }
    },
    hideLoginLoading (state) {
      return {
        ...state,
        loginLoading: false,
      }
    },
  },
}
